package com.learn.array;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/24/15.
 */
public class wildcardPatternMatching {

    public static void main(String[] args) {
        String s="sdfsdf";
        String p="?????????";

        IO.pln(isMatch(s, p));
    }

    private static boolean isMatch(String s, String p) {

        if(s.length()==0 && p.length()==0)
            return true;
        if(s.length()==0)
            return false;
        if(p.length()==0)
            return false;

        if( (s.charAt(0)==p.charAt(0) ) || (p.charAt(0)=='?'))
            return isMatch(s.substring(1),p.substring(1));
        else if(p.charAt(0)=='*') {
            return isMatch(s.substring(1),p.substring(1)) || isMatch(s.substring(1),p);
        }
        else
            return false;
    }
}
