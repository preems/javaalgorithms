package com.learn.array;

import com.learn.IO;

import java.util.HashSet;

/**
 * Created by Preetham MS <me@preetham.in> on 8/23/15.
 */
public class duplicateWithinKDistance {

    //http://www.geeksforgeeks.org/check-given-array-contains-duplicate-elements-within-k-distance/

    public static void main(String[] args) {
        int a[] = {1, 2, 3, 4, 4};
        Utils.printIntArray(a);

        HashSet<Integer> set = new HashSet<>();
        int k=3;
        int i;
        for(i=0;i<=k;i++){
         set.add(a[i]);
        }
        int j=0;

        while(i<a.length) {
            set.remove(a[j]);
            if(set.contains(a[i])){
                IO.pln(true);
                return;
            }

            else
                set.add(a[i]);
            i++;
            j++;
        }

        IO.pln(false);

    }
}
