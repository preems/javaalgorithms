package com.learn.array;

import com.learn.IO;

import java.util.Arrays;

/**
 * Created by Preetham MS <me@preetham.in> on 8/23/15.
 */
public class findPythagoreanTriplet {
    public static void main(String[] args) {
        int arr[] =  Utils.generateIntArray(10);
        Utils.printIntArray(arr);
        IO.pln(isTriplet(arr));
    }

    private static boolean isTriplet(int[] arr) {
        int n = arr.length;

        for(int i=0;i<n;i++){
            arr[i]=arr[i]*arr[i];
        }

        Arrays.sort(arr);


        for(int i=n-1;i>=2;i--) {
            int j=0;
            int k=i-1;

            while(j<k) {
                if(arr[j]+arr[k]>arr[i])
                    k--;
                else if(arr[j]+arr[k]<arr[i])
                    j++;
                else {
                    IO.pln(arr[i]);
                    IO.pln(arr[j]);
                    IO.pln(arr[k]);
                    return true;
                }

            }

        }

        return false;

    }
}
