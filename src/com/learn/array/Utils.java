package com.learn.array;

import com.learn.IO;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Preetham MS <me@preetham.in> on 8/6/15.
 */
public class Utils {

    //generate int array

    public static int[] generateIntArray(int n) {
        int[] a = new int[n];

        Random random = new Random();

        n--;
        while(n>=0) {

            a[n] = random.nextInt(20);
            n--;
        }

        return a;
    }

    public static int[] generateSortedIntArray(int n) {
        int a[] = generateIntArray(n);
        Arrays.sort(a);
        return a;
    }

    public static int[][] generate2dIntArray(int r,int c) {
        int[][] a = new int[r][c];

        Random random = new Random();

       for(int i=0;i<r;i++)
           for(int j=0;j<c;j++)
               a[i][j]=random.nextInt(20);

        return a;
    }

    public static void printIntArray(int[] a) {

        for ( int e : a)
            System.out.format("%2d  ",e);
        IO.pln("");
    }

    public static void print2dIntArray(int[][] a) {
        IO.pln("[");
        for( int [] arr : a) {
            printIntArray(arr);
        }
        IO.pln("]");
    }
}
