package com.learn.array;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/25/15.
 */
public class minSizeSubarraySum {
    //http://www.programcreek.com/2014/05/leetcode-minimum-size-subarray-sum-java/

    public static void main(String[] args) {
        int a[] = {2,3,1,2,4,2,1};
        int sum=7;


        int min=a.length;
        int i=0;
        int j=0;

        int subArraysum=a[0];
        while(j<a.length) {

            if(i==j) {
                if(a[i]>=sum){
                    IO.pln(1);
                    return;
                }
                else {
                    j++;
                    if (j < a.length) {
                        subArraysum += a[j];

                    } else {
                        IO.pln(min);
                        return;
                    }
                }
            }
            else{
                if(subArraysum>=sum){
                    min=Math.min(min,j-i+1);
                    subArraysum-=a[i];
                    i++;
                }
                else{
                    j++;
                    if(j<a.length) {
                        subArraysum += a[j];
                    }
                    else{
                            IO.pln(min);
                            return;
                        }
                    }
                }
            }

        IO.pln(i);
        IO.pln(j);
    }

}
