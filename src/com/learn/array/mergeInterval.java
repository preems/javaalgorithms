package com.learn.array;

import com.learn.IO;

import java.util.ArrayList;
import java.util.Comparator;

/**
 * Created by Preetham MS <me@preetham.in> on 8/24/15.
 */

class Interval {
    int start;
    int end;

    Interval(int start, int end) {
        this.start=start;
        this.end=end;
    }
}


public class mergeInterval {

    //http://www.programcreek.com/2012/12/leetcode-merge-intervals/

    public static void main(String[] args) {
        ArrayList<Interval> list = new ArrayList<>();
        ArrayList<Interval> result = new ArrayList<>();
        list.add(new Interval(1,3));
        list.add(new Interval(2,6));
        list.add(new Interval(15, 18));
        list.add(new Interval(8,10));

        list.sort(new intervalComparator());

        Interval prev = list.get(0);
        for(int i=1;i<list.size();i++) {
            Interval cur = list.get(i);
            if(list.get(i).start<=prev.end){
                Interval merged = new Interval(prev.start,Math.max(prev.end,cur.end));
                prev=merged;
            }
            else{
                result.add(prev);
                prev=cur;
            }
        }
        result.add(prev);


        for(Interval interval:result){
            IO.pln(interval.start+" "+interval.end);
        }

    }
}

class intervalComparator implements Comparator<Interval> {
    @Override
    public int compare(Interval a,Interval b){
        return a.start-b.start;
    }
}
