package com.learn.array;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/23/15.
 */
public class maxProfitByTwoShareTransactions {

    //http://www.geeksforgeeks.org/maximum-profit-by-buying-and-selling-a-share-at-most-twice/

    public static void main(String[] args) {
        int price[] = {2, 30, 15, 10, 8, 25, 80};
        int n = price.length;
        Utils.printIntArray(price);
                IO.pln(maxProfit(price, n));
    }

    private static int maxProfit(int[] price, int n) {
        int profit[] = new int[n];
        int max_price = price[n-1];


        for(int i=n-2;i>=0;i--) {
            if(price[i]>max_price) {
                max_price=price[i];
            }


            profit[i]= Math.max(profit[i+1],max_price-price[i]);
        }

        Utils.printIntArray(profit);
        int min_price = price[0];

        for(int i=1;i<n;i++) {
            if(min_price<price[i])
                min_price=price[i];
            profit[i]=Math.max(profit[i-1], price[i]-min_price+profit[i]);
        }

        Utils.printIntArray(profit);
        return profit[n-1];
    }
}
