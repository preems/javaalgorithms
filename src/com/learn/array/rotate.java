package com.learn.array;

import java.util.Arrays;
import java.util.Scanner;
/**
 * Created by preems on 4/6/15.
 */
public class rotate {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int[] arr = {1,2,3,4,5,6,7,8};
        System.out.println(rotatereverse(arr, 3));
    }

    public static String rotatek(int[] arr,int k) {
        int[] result = new int[arr.length];
        int n=arr.length;
        k=k%arr.length;
        int i;
        for (i=0;i<k;i++) {
            result[i]=arr[arr.length-k+i];
        }

        for (i=k;i<n;i++) {
            result[i]=arr[i-k];
        }

        return Arrays.toString(result);
    }

     public static int[] reverse(int[] a,int start,int end) {
         int i,temp;
         int n=end;
         for (i=start;i<(start+((end-start)/2));i++) {
             temp=a[i];
             a[i]=a[n-i+start];
             a[n-i+start]=temp;
         }
         return a;
     }

    public static String rotatereverse(int[] arr, int k) {
                int n = arr.length;
                arr = reverse(arr,0,k-1);
                arr = reverse(arr,k,n-1);
                arr = reverse(arr,0,n-1);
        return Arrays.toString(arr);
    }
}
