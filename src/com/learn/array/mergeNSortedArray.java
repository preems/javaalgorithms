package com.learn.array;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Created by Preetham MS <me@preetham.in> on 8/29/15.
 */
public class mergeNSortedArray {
    //Merge N sorted lists

    public static void main(String[] args) {
        int lists[][] = new int[5][10];
        for(int i=0;i<5;i++) {
            lists[i]=Utils.generateSortedIntArray(10);
            Utils.printIntArray(lists[i]);
        }

        int result[] = mergeLists(lists);
        Utils.printIntArray(result);

    }

    private static int[] mergeLists(int[][] lists) {
        int result[] = new int[50];

        int p[]=new int[5];

        Comparator<QueueNode> comparator = new Comparator<QueueNode>() {

            @Override
            public int compare(QueueNode a,QueueNode b) {
                return a.val-b.val;
            }
        };


        PriorityQueue<QueueNode> queue= new PriorityQueue<>(6,comparator);

        for(int i=0;i<5;i++) {
            queue.add(new QueueNode(lists[i][0],i));
        }

        int count=0;
        while(count<50) {
            QueueNode cur = queue.remove();
            result[count]=cur.val;
            p[cur.index]++;
            if(p[cur.index]<10)
                queue.add(new QueueNode(lists[cur.index][p[cur.index]],cur.index));
            count++;
        }
        return result;

    }

}

class QueueNode {
    int val;
    int index;

    QueueNode(int v,int i) {
        val=v;
        index=i;
    }
}
