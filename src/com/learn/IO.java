package com.learn;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */



public class IO {

    static BufferedReader reader;

    public IO() {
        reader = new BufferedReader(new InputStreamReader(System.in));

    }

    public static int getInt() {
        return Integer.parseInt(getLine());
    }

    public static String getLine() {
        try {
            return reader.readLine();
        }
        catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Double getDouble() {
        return Double.parseDouble(getLine());
    }

    public static int[] getIntArray() {
        String[] tokens = getLine().split(" ");
        int[] array = new int[tokens.length];
        for(int i =0;i<tokens.length;i++) {
            array[i] = Integer.parseInt(tokens[i]);
        }
        return array;
    }

    public static void p(String string) {
        System.out.print(string);
    }

    public static void p(int i) {
        System.out.print(i);
    }


    public static void pln(String string) {
        System.out.println(string);
    }


    public static void pln(int i) {
        System.out.println(i);
    }

    public static void pln(boolean b) { System.out.println(b);}

}