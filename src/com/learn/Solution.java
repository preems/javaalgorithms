import com.learn.IO;
import com.learn.array.Utils;
import com.sun.deploy.util.StringUtils;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;


class Node{
    int data;
    Node next;

    Node(int data){
        next=null;
        this.data=data;
    }
}

class Solution {

    public static void main(String[] args) {

        Comparator<Node> comparator = new Comparator<Node>() {
            @Override
            public int compare(Node o1, Node o2) {
                if(o1.data>o2.data)
                    return -1;
                else
                    return 1;
            }
        };

        PriorityQueue<Node> queue = new PriorityQueue<>(5,comparator);


        queue.add(new Node(10));

        queue.add(new Node(9));
        printQueue(queue);
        queue.add(new Node(8));
        printQueue(queue);
        queue.add(new Node(7));
        printQueue(queue);
        queue.add(new Node(6));
        printQueue(queue);
        queue.add(new Node(5));
        printQueue(queue);
        queue.add(new Node(4));
        printQueue(queue);
        queue.add(new Node(3));
        printQueue(queue);
        queue.add(new Node(2));
        printQueue(queue);
        queue.add(new Node(1));
        printQueue(queue);
        queue.add(new Node(11));
        printQueue(queue);

        while(!queue.isEmpty()) {
            IO.pln(queue.remove().data);
        }




    }

    private static void printQueue(Queue<Node> queue) {
        for(Node node: queue) {
            IO.p(node.data+" ");
        }
        IO.pln("");
    }

}
