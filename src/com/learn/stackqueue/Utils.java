package com.learn.stackqueue;

import com.learn.IO;

import java.util.Random;
import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 8/5/15.
 */
public class Utils {

    public static Stack<Integer> generateStack(int size) {
        Stack<Integer> stack = new Stack<>();
        Random random  = new Random();
        while (size>=0) {
            Integer a = random.nextInt(20);
            stack.push(a);
            size--;
        }

        return stack;

    }

    public static void printStack(Stack<Integer> stack) {
        Integer[] array = new Integer[stack.size()];
        stack.toArray(array);
        for (Integer a : array) {
            IO.p(a);
            IO.p("--");

        }
        IO.pln("");
    }
}
