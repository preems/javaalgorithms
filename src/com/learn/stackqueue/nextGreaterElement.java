package com.learn.stackqueue;

import com.learn.IO;
import com.learn.array.Utils;

import java.util.HashMap;
import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 8/6/15.
 */
public class nextGreaterElement {
    // http://www.geeksforgeeks.org/next-greater-element/

    public static void main(String[] args) {

        //int[] a = Utils.generateIntArray(10);
        int[] a = {13,7,6,12};

        Utils.printIntArray(a);

        HashMap<Integer,Integer> res = findNextGreaterElement(a);

        IO.p(res.toString());

    }

    private static HashMap<Integer, Integer> findNextGreaterElement(int[] a) {

        Stack<Integer> stack = new Stack<>();
        HashMap<Integer,Integer> res = new HashMap<>();


        for(int i=0;i<a.length;i++) {


            if(stack.empty())
            {
                stack.push(a[i]);
            }
            else {

                while(!stack.empty() && stack.peek()<a[i] ) {
                    int t = stack.pop();
                    if (t < a[i]) {
                        res.put(t, a[i]);
                    }
                }

                stack.push(a[i]);

            }


        }

        for( int e : a) {
            if(!res.containsKey(e)) {
                res.put(e,-1);
            }
        }

        return res;
    }
}
