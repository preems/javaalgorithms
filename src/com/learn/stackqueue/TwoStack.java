package com.learn.stackqueue;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/6/15.
 */
public class TwoStack {

    private int[] _arr;
    private int top1,top2;
    private int _size;

    TwoStack(int size) {
        _arr = new int[size];
        top1=-1;
        top2 = size;
        _size = size;
    }

    public void push1(int x) {

        if(top1+1<top2) {
            top1++;
            _arr[top1]=x;
        }
        else {
            IO.pln("Error inserting on stack 1");
        }
    }

    public int pop1() {
        if(top1>=0) {
            top1--;
            return _arr[top1+1];
        }
        else {
            IO.pln("Error pop on stack 1");
            return -1;
        }
    }

    public void push2(int x) {
        if(top2-1>top1) {
            top2--;
            _arr[top2]=x;
        }
        else {
            IO.pln("Error in inserting in stack 2");
        }
    }

    public int pop2() {
        if(top2<_size) {
            top2++;
            return _arr[top2-1];
        }
        else {
            IO.pln("Error pop on stack 2");
            return -1;
        }
    }

}

class Solution1 {
    public static void main(String[] args) {
        TwoStack stack = new TwoStack(10);
        stack.push1(10);
        stack.push1(10);        stack.push1(10);
        stack.push1(10);
        stack.push1(100);
        stack.push1(103);
        stack.push1(130);
        stack.push1(10);

        IO.pln(stack.pop1());
        IO.pln(stack.pop1());IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        IO.pln(stack.pop1());
        stack.push2(100);
        stack.push2(103);
        stack.push2(130);
        stack.push2(10);
        IO.pln(stack.pop2());
        IO.pln(stack.pop2());
        IO.pln(stack.pop2());
        IO.pln(stack.pop2());
        IO.pln(stack.pop2());


    }
}


