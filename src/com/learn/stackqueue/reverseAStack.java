package com.learn.stackqueue;

import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 8/5/15.
 */
public class reverseAStack {
    public static void main(String[] args) {
        Stack<Integer> stack = Utils.generateStack(10);
        Utils.printStack(stack);
        Stack<Integer> res = new Stack<>();
        stack=reverse(stack,res);
        Utils.printStack(stack);
    }

    private static Stack<Integer> reverse(Stack<Integer> stack, Stack<Integer> res) {

        if(stack.empty()) {
            return res;
        }

        Integer a = stack.pop();
        res.push(a);
        reverse(stack, res);

        return res;
    }

}
