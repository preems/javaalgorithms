package com.learn.stackqueue;

import com.learn.IO;

import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 8/10/15.
 */
public class SpecialStack extends Stack {
    //http://www.geeksforgeeks.org/design-and-implement-special-stack-data-structure/

    private Stack<Integer> minStack = new Stack<>();

    @Override
    public Object push(Object x) {
        super.push(x);

        if(minStack.empty()) {
            minStack.push((Integer)x);
        }
        else {
            if(minStack.peek()>(Integer)x) {
                minStack.push((Integer)x);
            }
        }
        return x;

    }

    public Integer getMin() {
        if(!minStack.empty())
            return minStack.peek();
        return -1;
    }

    @Override
    public Object pop() {
        Object ret = super.pop();

        if((!minStack.empty()) && minStack.peek()==ret) {
            minStack.pop();
        }
        return ret;
    }

}

class Solution {
    public static void main(String[] args) {
        SpecialStack stack = new SpecialStack();



        stack.push(100);
        stack.push(459);
        stack.push(10);
        stack.push(5);
        stack.push(1);

        IO.pln(stack.getMin());
        stack.pop();
        IO.pln(stack.getMin());
        stack.pop();
        IO.pln(stack.getMin());
        stack.pop();
        IO.pln(stack.getMin());
        stack.pop();
        IO.pln(stack.getMin());
        stack.pop();
        IO.pln(stack.getMin());
    }
}