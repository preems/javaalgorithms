package com.learn.string;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Preetham MS <me@preetham.in> on 8/14/15.
 */
public class Utils {

    public static String generateString(int length, int range) {
        Random random = new Random();
        char arr[] = new char[length];

        for(int i=0;i<length;i++)
            arr[i] = (char)(random.nextInt(range)+65);

        return String.valueOf(arr);

    }
}
