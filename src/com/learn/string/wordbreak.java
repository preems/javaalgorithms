package com.learn.com.learn.string;

import java.util.HashSet;
import java.util.Set;
/**
 * Created by preems on 4/6/15.
 */
public class wordbreak {
    public static void main(String[] args) {
        String s = "helloworldmr";
        HashSet<String> dict;
        dict = new HashSet<String>();
        dict.add("hello");
        dict.add("world");
        dict.add("mr");

        boolean[] t = new boolean[s.length()+1];

        t[0]=true;

        for (int i =0;i<s.length();i++) {
            if(!t[i]) continue;

            for(String a: dict) {
                int end=i+a.length();

                if (end>s.length())
                    continue;
                if (t[end])
                    continue;

                if(s.substring(i,end).equals(a))
                    t[end]=true;
            }
        }

        System.out.println(t[s.length()]);
    }
}
