package com.learn.com.learn.string;

/**
 * Created by preems on 4/5/15.
 */
public class possibleSpacedString {
    public static void main(String[] args) {
        possibleStrings("ABCDEF",1,6,0);
    }

    public static void possibleStrings(String str,int count,int length,int spaces){
        if (count>=length){
            System.out.println(str);
            return;
        }
        possibleStrings(str,count+1,length,spaces);
        if (count<str.length()) {
            possibleStrings(str.substring(0, count+spaces) + " " + str.substring(count+spaces), count + 1,length,spaces+1);
        }
    }
}
