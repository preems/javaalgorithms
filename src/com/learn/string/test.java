import java.io.*;

public class test {
    public static void main(String[] args) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String s;
        while ((s = in.readLine()) != null) {

            StringBuilder sb = new StringBuilder(s);
            //sb.reverse()
            sb.reverse();
            //sb.toString()

            String[] arr =sb.toString().split(" ");
            sb = new StringBuilder();

            for(String a: arr) {
                sb.append(a.substring(0,1).toUpperCase());
                sb.append(a.substring(1).toLowerCase());
                sb.append(" ");
            }

            System.out.println(sb.toString().trim());
        }
    }
}
