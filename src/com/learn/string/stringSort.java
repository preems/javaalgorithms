package com.learn.string;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/17/15.
 */
public class stringSort {

    //http://www.careercup.com/question?id=5647284379320320

    public static void main(String[] args) {
        String a = "a cBd LkmYsdfhsfIJ IHIijabIHB";
        IO.pln(a);
        a = sort(a);
        IO.pln(a);
    }

    private static String sort(String a) {
        char[] array = a.toCharArray();
        int n = array.length;
        for(int i=1;i<n;i++){
            for(int k=i;k>0;k--){
                if(compare(array[k-1],array[k])) {
                    char temp = array[k-1];
                    array[k-1]= array[k];
                    array[k]=temp;
                }
            }
        }
        return new String(array);

    }


    private static boolean compare(char a,char b) {

        if(Character.isUpperCase(a)) {
            if(Character.isLowerCase(b)) {
                return true;
            }
            else if(Character.isSpaceChar(b)){
                return true;
            }
        }
        else if(Character.isSpaceChar(a) && Character.isLowerCase(b)) {
                return true;
            }

        return false;
    }

}
