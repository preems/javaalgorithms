package com.learn.string;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by preems on 4/6/15.
 */
public class getStringFromTaggedString {

    public static void main(String[] args) {
        String str="Tree('S', [('At', 'IN'), ('eight', 'CD'), (\"o'clock\", 'JJ'),\n" +
                "           ('on', 'IN'), ('Thursday', 'NNP'), ('morning', 'NN'),\n" +
                "       Tree('PERSON', [('Arthur', 'NNP')]),\n" +
                "           ('did', 'VBD'), (\"n't\", 'RB'), ('feel', 'VB'),\n" +
                "           ('very', 'RB'), ('good', 'JJ'), ('.', '.')])";

        System.out.println(getString(str));
    }

    public static String getString(String str) {

        List<Character> result = new ArrayList();

        str= str.replaceAll("'","");

        boolean flag = false;
        for (char c : str.toCharArray()) {
            if (c=='(') {
                flag=true;
                }
            else if (c==',') {
                flag=false;
            }

            if (flag) result.add(c);
            }




        return result.toString();
    }
}
