package com.learn.graph;

import com.learn.IO;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

/**
 * Created by Preetham MS <me@preetham.in> on 8/24/15.
 */


class wordNode {
    String word;
    int steps;

    wordNode(String word,int steps){
    this.steps=steps;
        this.word=word;
    }
}

public class wordLadder {

    //http://www.programcreek.com/2012/12/leetcode-word-ladder/

    public static void main(String[] args) {
        String start = "hit";
        String end = "cog";
        Set<String> dict = new HashSet<>();
        dict.add("hot");
        dict.add("dot");
        dict.add("dog");
        dict.add("lot");
        dict.add("log");


        IO.pln(ladderLength(start,end,dict));
    }

    private static int ladderLength(String start, String end, Set<String> dict) {
        dict.add(end);
        Queue<wordNode> queue = new LinkedList<>();
        queue.add(new wordNode(start,0));

        while(!queue.isEmpty()) {
            wordNode cur = queue.remove();
            String word = cur.word;
            IO.pln(word);

            if(word.equals(end)){
                return cur.steps;
            }

            char a[]=word.toCharArray();
            for(int i=0;i<a.length;i++) {
                for(char c='a';c<='z';c++) {
                    char temp = a[i];
                    if(a[i]!=c){
                        a[i]=c;

                        if(dict.contains(new String(a))) {
                            queue.add(new wordNode(new String(a), cur.steps + 1));
                            dict.remove(new String(a));
                        }
                        a[i]=temp;
                    }

                }
            }


        }

        return 0;
    }

}
