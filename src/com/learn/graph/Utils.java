package com.learn.graph;

import com.learn.IO;

import java.util.Random;

/**
 * Created by Preetham MS <me@preetham.in> on 8/17/15.
 */
public class Utils {

    public static DirectedGraph generateGraph(int vertices, int edges ) {
        DirectedGraph graph = new DirectedGraph(vertices);


        Random random = new Random();
        while (edges>=0) {
            int start = random.nextInt(vertices);
            int end = random.nextInt(vertices);
            graph.addEdge(start,start==end?random.nextInt(vertices):end);
            edges--;
        }

        return graph;
    }
}
