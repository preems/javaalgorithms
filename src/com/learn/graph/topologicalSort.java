package com.learn.graph;

import com.learn.IO;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Preetham MS <me@preetham.in> on 8/17/15.
 */
public class topologicalSort {

    private static List<Integer> result;
    private static boolean[] visited;

    public static void main(String[] args) {

        result = new ArrayList<>();
        DirectedGraph graph = Utils.generateGraph(5,7);

        visited = new boolean[graph.n];
        for(int i=0;i<visited.length;i++){
            visited[i]=false;
        }
        graph.printDOT();

        sort(graph);
    }

    private static void sort(DirectedGraph graph) {

        for(int i=0;i<graph.n;i++) {
            if(visited[i]==false) {
                tsort(i,graph);
            }
        }

        Collections.reverse(result);
        for(int a: result) {
            IO.p(a+" ");
        }

    }

    private static void tsort(int i,DirectedGraph graph) {
        visited[i]=true;

        for(int a: graph.edges[i]) {
            if(visited[a]==false) {
                tsort(a,graph);
            }
        }
        result.add(i);
    }
}
