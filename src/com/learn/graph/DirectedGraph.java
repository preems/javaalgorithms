package com.learn.graph;

import com.learn.IO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Preetham MS <me@preetham.in> on 8/17/15.
 */
public class DirectedGraph {

    public List<Integer>[] edges;
    int n;

    DirectedGraph(int verticies) {
        edges = new ArrayList[verticies];
        n=verticies;

        for(int i=0;i<verticies;i++)
            edges[i]=new ArrayList<>();

    }


    public void addEdge(int start, int end) {
        edges[start].add(end);
    }

    public void print(){
        for(int i=0;i<n;i++){
            IO.p(i+": ");
            for(Integer e: edges[i]) {
                IO.p(e+" ");
            }
            IO.pln("");
        }
    }

    public void printDOT() {
        IO.pln("digraph graphname {");

        for(int i=0;i<n;i++){
            for(Integer e: edges[i]) {
                IO.pln(i+" -> "+e+";");
            }
        }

        IO.pln("}");
    }
}
