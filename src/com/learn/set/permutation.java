package com.learn.com.learn.set;

import java.util.Arrays;
/**
 * Created by preems on 4/5/15.
 */
public class permutation {
    public static void permute(char[] a,int n) {
        if (a.length==n) {
            System.out.println(Arrays.toString(a));
        }

        int i;
        char temp;
        for (i=n;i<a.length;i++) {
            //swap(a[i],a[n]);
            temp=a[i];
            a[i]=a[n];
            a[n]=temp;

            char[] a1 = Arrays.copyOf(a,a.length);
            permute(a1,n+1);

            //temp=a[n];
            //a[n]=a[i];
            //a[i]=temp;
        }
    }

    public static void swap(char a,char b) {
        char temp=a;
        a=b;
        b=temp;
    }

    public static void main(String[] args) {
        permute("abc".toCharArray(),0);
    }
}
