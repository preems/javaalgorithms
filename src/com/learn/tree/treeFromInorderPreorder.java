package com.learn.tree;

/**
 * Created by Preetham MS <me@preetham.in> on 8/27/15.
 */
public class treeFromInorderPreorder {

    public static Node buildTree(int[] preOrder, int preStart,int preEnd, int[] inOrder, int inStart,int inEnd) {


        if(preStart>preEnd || inStart>inEnd)
            return null;


        int data=preOrder[preStart];
        Node temp = new Node(data);
        int i;
        for(i=inStart;i<=inEnd;i++){
            if(inOrder[i]==data)
                break;
        }

        temp.left=buildTree(preOrder,preStart+1,preStart+i-inStart,inOrder,inStart,i-1);
        temp.right=buildTree(preOrder,preStart+i+1-inStart,preEnd,inOrder,i+1,inEnd);
        return temp;
    }

    public static void main(String[] args) {
        int preOrder[] ={1,2,4,5,3,7,6,8};
        int inOrder[] = {4,2,5,1,6,7,3,8};
        Node root = buildTree(preOrder,0,7,inOrder,0,7);
        Utils.print(root);
    }

}


