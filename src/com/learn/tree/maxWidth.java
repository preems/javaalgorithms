package com.learn.tree;

import com.learn.IO;
import com.sun.tools.corba.se.idl.constExpr.Not;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Preetham MS <me@preetham.in> on 8/12/15.
 */
public class maxWidth {
    //http://www.geeksforgeeks.org/maximum-width-of-a-binary-tree/

    public static void main(String[] args) {
        Node root = Utils.generateImbalancedTree(5);
        Utils.print(root);
        int a = getMaxWidth(root);
        IO.pln(getMaxWidth2(root));
        IO.pln(a);
    }

    private static int getMaxWidth(Node root) {
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        queue.add(null);
        int maxWidth=0;
        int curWidth=0;
        Node cur;
        while(!queue.isEmpty()) {
            cur=queue.remove();
            if(cur==null) {
                curWidth=0;
                if(!queue.isEmpty()) queue.add(null);
            }
            else {
                curWidth++;

                if(curWidth>maxWidth)
                    maxWidth=curWidth;

                if(cur.left!=null) queue.add(cur.left);
                if(cur.right!=null) queue.add(cur.right);

            }
        }
        return maxWidth;

    }

    private static int getWidthAtLevel(Node root,int level) {
        if(level<1) return 0;
        if(root==null) return 0;
        if(level==1) return 1;
        else
            return getWidthAtLevel(root.left,level-1)+getWidthAtLevel(root.right,level-1);
    }

    private static int getMaxWidth2(Node root) {
        int maxHeight=0;
        int curHeight;
        for(int i=1;i<=getHeight(root);i++) {
            curHeight=getWidthAtLevel(root,i);
            if (curHeight>maxHeight)
                maxHeight=curHeight;
        }

        return maxHeight;
    }

    private static int getHeight(Node root) {
        if(root==null)
            return 0;
        else
            return getHeight(root.left)+getHeight(root.right)+1;
    }
}
