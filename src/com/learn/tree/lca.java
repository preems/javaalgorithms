package com.learn.tree;

import com.learn.IO;
import com.learn.tree.Node;
import com.learn.tree.Utils;
/**
 * Created by preems on 4/6/15.
 */
public class lca {

    public static Node getLca(Node root,int n1,int n2) {
        if (root==null) {
            return null;
        }

        if ( root.data==n1 || root.data==n2 ) {
            return root;
        }

        Node l = getLca(root.left,n1,n2);
        Node r=getLca(root.right,n1,n2);

        if (l!=null && r!=null) {
            return root;
        }

        if (l==null) {
            return r;
        }
        else {
            return l;
        }
    }

    public static Node getBstLCA(Node root, int n1, int n2) {
        while(root!=null) {
            if (root.data<n1 && root.data<n2) {
                root=root.left;
            }
            else if (root.data>n1 && root.data>n2) {
                root=root.right;
            }
            else {
                return root;
            }
        }
        return root;
    }

    public static void main(String[] args) {
        Node root = Utils.generateTree(4);
        Utils.print(root);
        Node res = getLca(root, 5, 8);
        if(res!=null)
            System.out.println(res.data);
        else
            System.out.println("LCA does not exist");

        //root=Utils.generateBST(5);
        //Utils.print(root);
        //System.out.println(bstlca(root, 7, 11).data);
    }


    public static Node bstlca(Node root, int a, int b) {

        while(root!=null) {
            if(root.data> a && root.data >b) {
                root=root.left;
            }
            else if(root.data<a && root.data<b) {
                root=root.right;
            }
            else {
                return root;
            }
        }
        return root;

    }

}
