package com.learn.tree;

import com.learn.IO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Preetham MS <me@preetham.in> on 8/11/15.
 */
public class printAllPaths {
    //http://www.geeksforgeeks.org/given-a-binary-tree-print-out-all-of-its-root-to-leaf-paths-one-per-line/

    public static void main(String[] args) {
        Node root = Utils.generateTree(4);
        Utils.print(root);
        List<Integer> path = new ArrayList<>();
        printpaths(root, path);

    }

    private static void printpaths(Node root, List<Integer> path) {

        if(root==null) return;

        if(root.right==null && root.left==null) {
            List<Integer> newpath = new ArrayList<>(path);
            newpath.add(root.data);
            IO.pln(newpath.toString());
            return;
        }

        List<Integer> newpath = new ArrayList<>(path);
        newpath.add(root.data);
        printpaths(root.left, newpath);
        printpaths(root.right,newpath);



    }

}
