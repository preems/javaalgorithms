package com.learn.tree;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by preems on 4/9/15.
 */
public class levelOrder {
    public static void main(String[] args) {
        Node root = Utils.generateTree(5);
        Utils.print(root);
        levelOrderTraversal( root);

    }

    public static void levelOrderTraversal(Node root) {
        Queue<Node> q = new LinkedList<Node>();

        Node node;
        q.add(root);
        q.add(null);

        while(!q.isEmpty()) {
            node = q.remove();

            if (node==null) {
                System.out.println();
                if (!q.isEmpty()) q.add(null);
            }
            else {

                System.out.print(node.data + " ");

                if (node.left != null) q.add(node.left);
                if (node.right != null) q.add(node.right);
            }
        }


    }
}
