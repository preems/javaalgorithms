package com.learn.tree;


/**
 * Created by preems on 4/9/15.
 */
public class isBalanced {
    public static void main(String[] args) {
        Node root = Utils.generateImbalancedTree(4);
        Utils.print(root);

        System.out.print(isBal(root)==-1?false:true);
    }

    private static int isBal(Node root) {
        if(root==null) {
            return 0;
        }

        int lheight = isBal(root.left);
        if(lheight==-1) return -1;

        int rheight = isBal(root.right);
        if(rheight==-1) return -1;

        if(Math.abs(lheight-rheight) >1 ) return -1;

        return Math.max(lheight,rheight)+1;
    }
}
