package com.learn.tree;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/17/15.
 */
public class DFS {
    // Plain DFS traversal on a tree

    public static void main(String[] args) {
        Node root = Utils.generateTree(3);
        Utils.print(root);
        dfs(root);

    }

    private static void dfs(Node root) {

        if(root!=null) {

            if(root.left!=null) dfs(root.left);
            if(root.right!=null) dfs(root.right);

            IO.p(root.data+" ");
        }

    }

}
