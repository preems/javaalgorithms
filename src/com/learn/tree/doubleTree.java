package com.learn.tree;

/**
 * Created by Preetham MS <me@preetham.in> on 8/12/15.
 */
public class doubleTree {

    //http://www.geeksforgeeks.org/double-tree/

    public static void main(String[] args) {
        Node root = Utils.generateTree(3);
        Utils.print(root);
        root = makeDouble(root);
        Utils.print(root);
    }

    private static Node makeDouble(Node root) {
        if(root==null) {
            return null;
        }

        Node node = new Node(root.data);
        node.left=root.left;
        root.left=node;
        makeDouble(root.left.left);
        return root;
    }
}
