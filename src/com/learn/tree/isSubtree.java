package com.learn.tree;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/15/15.
 */
public class isSubtree {
    //http://www.geeksforgeeks.org/check-binary-tree-subtree-another-binary-tree-set-2/

    public static void main(String[] args) {
        Node root = Utils.generateTree(5);
        //Node subtree = root.right.right;
        Node subtree=Utils.generateTree(2);

        Utils.print(root);
        IO.pln("");
        Utils.print(subtree);
        IO.pln(reccursiveIsSubtree(root,subtree));
    }

    private static boolean reccursiveIsSubtree(Node root, Node subtree) {
        if(root==null && subtree==null) {
            return true;
        }
        else if(root==null || subtree==null) {
            return false;
        }
        else if(root.data==subtree.data) {
            return reccursiveIsSubtree(root.left,subtree.left) && reccursiveIsSubtree(root.right,subtree.right);
        }
        else {
            return reccursiveIsSubtree(root.left,subtree) || reccursiveIsSubtree(root.right,subtree);
        }



    }


}
