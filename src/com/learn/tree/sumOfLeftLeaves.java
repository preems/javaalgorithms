package com.learn.tree;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/13/15.
 */
public class sumOfLeftLeaves {

    //http://www.geeksforgeeks.org/find-sum-left-leaves-given-binary-tree/
    public static void main(String[] args) {
        Node root = Utils.generateTree(4);
        Utils.print(root);
        IO.pln(sum(root,0));
    }

    private static int sum(Node root, int type) {
        if(root==null)
            return 0;

        if(root.left==null && root.right==null && type==0)
            return root.data;

        return sum(root.left,0)+sum(root.right,1);

    }
}
