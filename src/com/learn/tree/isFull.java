package com.learn.tree;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/13/15.
 */
public class isFull {
    //http://www.geeksforgeeks.org/check-whether-binary-tree-full-binary-tree-not/

    public static void main(String[] args) {
        Node root = Utils.generateImbalancedTree(3);
        Utils.print(root);
        IO.pln(isfull(root));
    }

    private static boolean isfull(Node root) {
        if(root==null)
            return true;
        if(root.right==null && root.left!=null)
            return false;
        if(root.left==null && root.right!=null)
            return false;
        return isfull(root.left) && isfull(root.right);
    }

}
