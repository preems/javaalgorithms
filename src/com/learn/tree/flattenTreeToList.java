package com.learn.tree;

import com.learn.IO;

import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 8/27/15.
 */
public class flattenTreeToList {

    //http://www.programcreek.com/2013/01/leetcode-flatten-binary-tree-to-linked-list/


    public static Node flatten(Node root) {
        Stack<Node> stack = new Stack<>();

        Node cur=root;
        Node prev;
        while((!stack.empty()) || cur!=null) {

            if(cur.right!=null)
            {
                stack.push(cur.right);

            }

            prev=cur;
            if(cur.left!=null) {
                cur.right=cur.left;
                cur=cur.right;
            }
            else{
                if(!stack.empty())
                    cur.right=stack.pop();
                    cur=cur.right;
            }


        }

        return root;
    }

    public static void main(String[] args) {
        Node root = Utils.generateTree(3);
        Utils.print(root);
        root=flatten(root);

        while(root!=null)
        {
            IO.p(root.data+" ");
            root=root.right;
        }

    }

}
