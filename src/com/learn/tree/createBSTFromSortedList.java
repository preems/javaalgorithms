package com.learn.tree;

import com.learn.linkedlist.*;
import com.learn.linkedlist.Node;
import com.learn.linkedlist.Utils;

/**
 * Created by Preetham MS <me@preetham.in> on 8/28/15.
 */
public class createBSTFromSortedList {

    static Node head;

    public static void main(String[] args) {
        head= com.learn.linkedlist.Utils.generateSortedList(10);

        int len = Utils.length(head);
        Utils.printList(head);
        com.learn.tree.Node root = BSTFromSortedList(0,len-1);

        com.learn.tree.Utils.print(root);
    }

    private static com.learn.tree.Node BSTFromSortedList( int start, int end) {

        if(start>end)
            return null;

        int mid=(start+end)/2;

        com.learn.tree.Node left = BSTFromSortedList(start,mid-1);
        com.learn.tree.Node root = new com.learn.tree.Node(head.data);
        head=head.next;
        com.learn.tree.Node right = BSTFromSortedList(mid+1,end);

        root.left=left;
        root.right=right;

        return root;


    }

}
