package com.learn.tree;

import com.sun.jmx.remote.internal.ArrayQueue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Random;

/**
 * Created by preems on 4/8/15.
 */
public class Utils {

    private static Random rand = new Random();

    private static Queue<Node> q = new LinkedList<Node>();


    public static Node generateTree(int levels) {

        if (levels==0) {
            return null;
        }

        Node root = new Node(rand.nextInt(19));
        root.left = generateTree(levels-1);
        root.right=generateTree(levels - 1);

        return root;
    }

    public static Node generateImbalancedTree(int levels) {
        //if(completed==true) return generateTree(levels-1);

        if(levels==0) {
            return null;
        }

        Node root = new Node(rand.nextInt(30));
        if (rand.nextInt(11)>9)
        {
            root.left=null;
        }
        else{
            root.left=generateImbalancedTree(levels-1);
        }
        if (rand.nextInt(11)<2)
        {
            root.right=null;
        }
        else{
            root.right=generateImbalancedTree(levels-1);
        }
        return root;
    }



    public static Node generateBST(int levels) {
        int n = (int)Math.pow(2,levels)-1;
        int[] arr = new int[n];
        for(int i=1;i<=n;i++){
            arr[i-1]=i;
        }

        Node root = generateBST_helper(arr, 0, n-1);

        return root;
    }

    private static Node generateBST_helper(int[] arr, int start, int end) {
        if(start>end) {
            return null;
        }

        int mid=(start+end)/2;
        Node node = new Node(arr[mid]);
        node.left=generateBST_helper(arr, start, mid - 1);
        node.right=generateBST_helper(arr,mid+1,end);
        return node;
    }

    public static void printTree(Node root) {
        q.add(root);
        q.add(null);
        while(!q.isEmpty()) {
            Node node = q.remove();

            if(node==null) {
                System.out.println("");
                if(!q.isEmpty()) q.add(null);
            }
            else {
                System.out.print(node.data);
                if (node.left != null) {
                    q.add(node.left);
                }
                if (node.right != null) {
                    q.add(node.right);
                }
            }

        }
    }

    public static void main(String[] args) {
        Node root = generateTree(3);
        //printTree(root);

        print(root);
    }

    public static void print(Node root) {
        printHelper(root, "");
    }

    private static void printHelper(Node root, String indent) {
        //http://www.cs.toronto.edu/~hojjat/148s07/labs/11-bst/BST.java
        if (root == null) {
            //System.out.println(indent + "null");
            return;
        }

        // Pick a pretty indent.
        String newIndent;
        if (indent.equals("")) {
            newIndent = ".. ";
        }
        else {
            newIndent = "..." + indent;
        }

        printHelper(root.right, newIndent);
        System.out.println(indent + root.data);
        printHelper(root.left, newIndent);
    }

    public static int count(Node root) {
        if(root==null) {
            return 0;
        }
        else {
            return count(root.right)+count(root.left)+1;
        }
    }

}
