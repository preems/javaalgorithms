package com.learn.tree;

/**
 * Created by Preetham MS <me@preetham.in> on 8/28/15.
 */
public class treeFromInorderPostOrder {

    public static Node buildTree(int postOrder[], int postStart, int postEnd, int inOrder[], int inStart, int inEnd)
    {
        if(postStart>postEnd || inStart>inEnd) {
            return null;
        }

        int data = postOrder[postEnd];
        Node temp = new Node(data);

        int i;
        for(i=inStart;i<=inEnd;i++){
            if(inOrder[i]==data)
                break;
        }

        temp.left=buildTree(postOrder,postStart,postStart+i-inStart-1,inOrder,inStart,i-1);
        temp.right=buildTree(postOrder,postStart+i-inStart,postEnd-1,inOrder,i+1,inEnd);
        return temp;
    }

    public static void main(String[] args) {
        int postOrder[] ={4,5,2,6,7,8,3,1};
        int inOrder[] = {4,2,5,1,6,7,3,8};
        Node root = buildTree(postOrder,0,7,inOrder,0,7);
        Utils.print(root);
    }
}
