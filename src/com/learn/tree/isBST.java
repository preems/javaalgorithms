package com.learn.tree;

import com.learn.IO;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by preems on 4/13/15.
 */

public class isBST {

    private static List<Integer> inorder;

    public static void main(String[] args) {

        boolean flag=true;
        inorder = new ArrayList<>();
        Node root = Utils.generateBST(5);
        Utils.print(root);

        isInorderBST(root);

        for(int i=1;i<inorder.size();i++) {
            if(inorder.get(i-1)>=inorder.get(i)) {
                IO.pln(false);
                flag=false;
                break;
            }

        }
        if(flag==true)
            IO.pln(true);


        IO.pln(isReccursiveBST(root, -1, 1000000));
        isIterativeBST(root);
    }

    private static boolean isReccursiveBST(Node root, int min, int max) {

        if(root==null)
            return true;

        if(root.data>min && root.data<max) {
            return isReccursiveBST(root.left,min,root.data) && isReccursiveBST(root.right,root.data,max);
        }
        return false;
    }

    private static void isInorderBST(Node root) {
        if(root==null)
        {
            return;
        }
        isInorderBST(root.left);
        inorder.add(root.data);
        isInorderBST(root.right);
    }


    private static void isIterativeBST(Node root) {
        List<Integer> list = new ArrayList<>();
        Stack<Node> stack = new Stack<>();
        Node cur = root;

        //inorder
        while(cur!=null || (!stack.empty())) {
            while(cur!=null){
                stack.push(cur);
                cur=cur.left;
            }
            cur=stack.pop();
            list.add(cur.data);
            cur=cur.right;
        }

        for(int i=1;i<list.size();i++) {
            if(list.get(i-1)>=list.get(i)) {
                IO.pln(false);
                return;
            }
        }

        IO.pln(true);
    }
}
