package com.learn.tree;

import com.learn.IO;

import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 8/18/15.
 */
public class treeTraversal {

    public static void main(String[] args) {
        Node root = Utils.generateBST(4);
        Utils.print(root);
        inorderTraversal(root);
        IO.pln(" ");
        preorderTraversal(root);
    }

    private static void inorderTraversal(Node root) {
        Stack<Node> stack = new Stack<>();

        Node cur=root;
        while((!stack.empty()) || cur!=null){
            if(cur!=null) {
                stack.push(cur);
                cur=cur.left;
            }
            else{
                cur=stack.pop();
                IO.p(cur.data+" ");
                cur=cur.right;
            }
        }

    }


    private static void preorderTraversal(Node root) {
        Stack<Node> stack = new Stack<>();

        stack.push(root);
        while(!stack.empty()) {
            if(root.right!=null) stack.push(root.right);
            IO.p(root.data+" ");
            root=root.left;

            if(root==null && (!stack.empty()))
                root=stack.pop();
        }

    }


}
