package com.learn.tree;

import com.learn.IO;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Preetham MS <me@preetham.in> on 8/13/15.
 */
public class modifiedLevelOrder {
    //http://www.geeksforgeeks.org/perfect-binary-tree-specific-level-order-traversal/


    public static void main(String[] args) {
        Node root = Utils.generateTree(4);
        Utils.print(root);
        levelOrder(root);

    }


    public static void levelOrder(Node root) {
        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        Node a,b;
        while(!queue.isEmpty()) {
            if(queue.size()==1) {
                a=queue.remove();
                IO.p(a.data+" ");
                if(a.left!=null) queue.add(a.left);
                if(a.right!=null) queue.add(a.right);
            }
            else {
                a=queue.remove();
                b=queue.remove();
                IO.p(a.data+" ");
                IO.p(b.data+" ");
                if(a.left!=null) queue.add(a.left);
                if(b.right!=null) queue.add(b.right);
                if(a.right!=null) queue.add(a.right);
                if(b.left!=null) queue.add(b.left);
            }
        }

    }
}
