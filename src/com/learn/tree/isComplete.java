package com.learn.tree;

import com.learn.IO;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 8/10/15.
 */
public class isComplete {
    public static void main(String[] args) {
        //http://www.geeksforgeeks.org/check-if-a-given-binary-tree-is-complete-tree-or-not/

        Node root = Utils.generateTree(5);
        Utils.print(root);
        IO.pln(iterativeIsComplete(root));
        IO.pln(reccursiveIsComplete(root,0,Utils.count(root)));
    }

    private static boolean reccursiveIsComplete(Node root,int count,int size) {

        if(root==null) {
            return true;
        }
        else if(count>size) {
            return false;
        }
        else
            return reccursiveIsComplete(root.left,count*2+1,size) && reccursiveIsComplete(root.right,count*2+2,size);

    }

    private static boolean iterativeIsComplete(Node root) {

        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        Node cur;
        boolean leafNodeFound = false;
        while(!queue.isEmpty()){
            cur = queue.remove();

            if(cur.left!=null) {
                if(leafNodeFound) return false;
                else {
                    queue.add(cur.left);
                }
            }
            else {
                leafNodeFound=true;
            }

            if(cur.right!=null) {
                if (leafNodeFound) return false;
                else {
                    queue.add(cur.right);
                }
            }
            else {
                leafNodeFound=true;
            }

        }

        return true;
    }


}
