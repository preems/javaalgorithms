package com.learn.tree;


import com.learn.IO;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Preetham MS <me@preetham.in> on 8/29/15.
 */
public class linkedListOfLevels {
    //Create a LinkedList of all nodes at each depth.

    static ArrayList<LinkedList<Integer>> lists;
    static int curList=0;

    public static void main(String[] args) {
        lists = new ArrayList<>();
        lists.add(new LinkedList<>());
        Node root = Utils.generateBST(3);
        Utils.print(root);
        createLists(root, lists);
        for(LinkedList<Integer> list:lists){
            for(Integer integer:list){
                IO.p(integer+ " ");
            }
            IO.pln(" ");
        }
    }

    private static void createLists(Node root, ArrayList<LinkedList<Integer>> lists) {
        if(root==null)
            return;

        Queue<Node> queue = new LinkedList<>();
        queue.add(root);
        queue.add(null);
        while(!queue.isEmpty()) {
            Node cur = queue.remove();
            if(cur==null){
                lists.add(new LinkedList<>());
                curList++;
                if(!queue.isEmpty()) queue.add(null);

            }
            else
            {
                lists.get(curList).add(cur.data);
                if(cur.left!=null) queue.add(cur.left);
                if(cur.right!=null) queue.add(cur.right);
            }
        }
    }

}
