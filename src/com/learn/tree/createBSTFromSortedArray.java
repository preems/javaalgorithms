package com.learn.tree;

/**
 * Created by preems on 4/13/15.
 */
public class createBSTFromSortedArray {
    public static void main(String[] args) {
        int[] a ={1,2,3,4,5,6,7,8,9,10,11,12,13};
        Node root = CreateTree(a,0,12);
        Utils.print(root);
    }

    public static Node CreateTree(int[] arr, int start, int end) {
        if(start>end) {
            return null;
        }

        int mid=(start+end)/2;
        Node node = new Node(arr[mid]);
        node.left=CreateTree(arr,start,mid-1);
        node.right=CreateTree(arr,mid+1,end);

        return node;
    }
}
