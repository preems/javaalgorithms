package com.learn.tree;

import com.learn.IO;

import java.util.PriorityQueue;

/**
 * Created by Preetham MS <me@preetham.in> on 8/11/15.
 */
public class childrenSum {

    //http://www.geeksforgeeks.org/check-for-children-sum-property-in-a-binary-tree/

    public static void main(String[] args) {
        Node root = Utils.generateTree(2);
        Utils.print(root);
        IO.pln(checkChildrensum(root));
    }

    private static boolean checkChildrensum(Node root) {
        if(root==null) {
            return true;
        }

        if(root.right==null && root.left==null)
            return true;

        checkChildrensum(root.right);
        checkChildrensum(root.left);

        int left = root.left==null?0:root.left.data;
        int right = root.right==null?0:root.right.data;

        if( (left+right==root.data) &&  checkChildrensum(root.right) && checkChildrensum(root.left) )
            return true;
        else
            return false;


    }
}
