package com.learn.tree;

import com.learn.IO;
import com.sun.tools.corba.se.idl.constExpr.Not;

/**
 * Created by Preetham MS <me@preetham.in> on 8/11/15.
 */
public class mirrorTree {
    public static void main(String[] args) {
        //http://www.geeksforgeeks.org/write-an-efficient-c-function-to-convert-a-tree-into-its-mirror-tree/


        Node root = Utils.generateImbalancedTree(3);
        Utils.print(root);
        root = mirror(root);
        IO.pln("");
        Utils.print(root);

    }

    private static Node mirror(Node root) {
        if(root==null) {
            return null;
        }

        mirror(root.left);
        mirror(root.right);

        Node temp = root.left;
        root.left=root.right;
        root.right=temp;

        return root;

    }
}
