package com.learn.tree;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/13/15.
 */
public class removeHalfNodes {
    public static void main(String[] args) {
        //http://www.geeksforgeeks.org/given-a-binary-tree-how-do-you-remove-all-the-half-nodes/

        Node root = Utils.generateImbalancedTree(4);
        Utils.print(root);
        IO.pln("");
        root = remove(root);
        Utils.print(root);
    }

    private static Node remove(Node root) {
        if(root==null)
            return null;

        if(root.right==null && root.left!=null) {
            //root.right=root.left.right;
            //root.left=root.left.left;
            root=root.left;
            root=remove(root);
        }
        else if(root.left==null && root.right!=null) {
            //root.left=root.right.left;
            //root.right=root.right.right;
            root=root.right;
            root=remove(root);
        }

        root.left=remove(root.left);
        root.right=remove(root.right);
        return root;

    }
}
