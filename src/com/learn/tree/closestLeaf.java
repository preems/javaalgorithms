package com.learn.tree;

import com.learn.IO;

import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 8/13/15.
 */
public class closestLeaf {

    private static Stack<Node> ancestors;

    public static void main(String[] args) {
        //http://www.geeksforgeeks.org/find-closest-leaf-binary-tree/

        //This solution does not work. Needs to be fixed.

        ancestors = new Stack<>();
        Node root = Utils.generateImbalancedTree(6);
        root.right.left=null;
        root.right.right=null;
        Utils.print(root);
        IO.pln(findClosest(root.left,root));

    }

    private static int findClosest(Node key,Node root) {

        //DFS
        Stack<Node> stack = new Stack();
        Stack<Node> ancestors = new Stack();
        stack.add(root);
        Node cur;
        while(!stack.isEmpty()) {
            cur=stack.pop();
            ancestors.push(cur);

            if(cur==key) {
                ancestors.push(cur);
                return closest(root,ancestors);
            }

            if(cur.left!=null) stack.push(cur.left);
            if(cur.right!=null) stack.push(cur.right);

        }

        return -1;
    }

    private static int closest(Node root, Stack<Node> stack) {
        int minDistance=1000000000;
        int curDistance;



        for (Node node : stack) {
            curDistance=findDistanceToClosestLeaf(node);
            if (curDistance<minDistance) {
                minDistance=curDistance;
            }
        }


        return minDistance;

    }

    private static int findDistanceToClosestLeaf(Node root) {
        if(root==null)
            return 0;
        if((root.left==null) && (root.right==null))
            return 0;
        else
            return Math.min(findDistanceToClosestLeaf(root.left),findDistanceToClosestLeaf(root.right))+1;
    }
}
