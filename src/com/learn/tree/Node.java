package com.learn.tree;

/**
 * Created by preems on 4/6/15.
 */
public class Node {
    public Node right;
    public Node left;
    public int data;

    public Node(int data) {
        this.data=data;
        this.right=null;
        this.left=null;
    }
}
