package com.learn.heap;

import org.junit.Assert;

/**
 * Created by Preetham MS <me@preetham.in> on 8/25/15.
 */
public class HeapTest {

    // This is for testing the int heap implementation in this package.
    public static void main(String[] args) {

        Heap heap = new Heap();

        for(int i=0;i<20;i++){
            heap.add(i);
        }

        Assert.assertEquals(heap.size(), 20);
        Assert.assertEquals(heap.remove(), (Integer) 19);
        Assert.assertEquals(heap.remove(), (Integer) 18);
        Assert.assertEquals(heap.remove(), (Integer) 17);
        Assert.assertEquals(heap.remove(),(Integer)16);
        Assert.assertEquals(heap.remove(),(Integer)15);
        Assert.assertEquals(heap.size(), 15);

    }
}
