package com.learn.heap;

import com.learn.IO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Preetham MS <me@preetham.in> on 8/25/15.
 */
public class Heap {

    private List<Integer> a;

    Heap() {
        a=new ArrayList<>();
    }


    private int par(int i) {
        return (i-1)/2;
    }

    private int left(int i) {
        if( (2*i+1) <a.size() )
            return 2*i+1;
        else
            return -1;
    }

    private int right(int i) {
        if( (2*i+2) <a.size() )
            return 2*i+2;
        else
            return -1;
    }

    public void add(int x) {
        a.add(a.size(),x);

        int pos=a.size()-1;

        while(pos>0) {
            int parent = par(pos);
            if(a.get(parent)<a.get(pos)) {
                Integer temp = a.get(pos);
                a.set(pos,a.get(parent));
                a.set(parent,temp);
                pos=parent;
            }
            else{
                break;
            }
        }
    }


    public Integer remove() {
        Integer ret =a.get(0);
        if(a.size()==1) {
            a.remove(0);
            return ret;
        }
        a.set(0,a.remove(a.size() - 1));
        int pos=0;
        int highestChild;
        while(pos!=-1) {

            if(left(pos)!=-1){
                highestChild=left(pos);
            }
            else{
                break;
            }

            if(right(pos)!=-1 ) {
                if(a.get(right(pos)) > a.get(left(pos)))
                {
                    highestChild=right(pos);
                }
            }
            if(a.get(highestChild)>a.get(pos)) {
                Integer temp = a.get(pos);
                a.set(pos,a.get(highestChild));
                a.set(highestChild,temp);
                pos=highestChild;
            }else{
                break;
            }
        }


        return ret;
    }

    public int size() {
        return a.size();
    }

    public boolean isEmpty() {
        return a.isEmpty();
    }

    private void printInternalList(){
        //Use this for debug only
        for(Integer integer :a) {
            IO.p(integer+" ");
        }
        IO.pln(" ");
    }

}
