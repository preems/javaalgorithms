package com.learn.problem;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * Created by Preetham MS <me@preetham.in> on 8/18/15.
 */
public class simpleDatabase {
    //https://www.thumbtack.com/challenges/simple-database#io

    private static HashMap<String,String> mainMap;
    private static HashMap<String,Integer> mainMap_reverse;
    private static HashMap<String,String>[] tempMaps;
    private static HashMap<String,Integer>[] tempMaps_reverse;
    private static HashMap<String,String> curMap;
    private static HashMap<String,Integer> curMap_reverse;
    private static int curTempMap=-1;


    public static void main(String[] args) {

        mainMap = new HashMap<>();
        mainMap_reverse = new HashMap<>();
        tempMaps = new HashMap[10];
        tempMaps_reverse = new HashMap[10];

        for(int i=0;i<10;i++){
            tempMaps[i]=new HashMap<>();
            tempMaps_reverse[i]=new HashMap<>();
        }
        curMap = mainMap;
        curMap_reverse=mainMap_reverse;

        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String command;
            while ((command = reader.readLine()) != null) {
                if (command.equals("EXIT"))
                    System.exit(0);
                else
                    process_command(command);
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void process_command(String command) {
        String[] commands = command.split(" ");

        if(commands[0].equals("SET")) {
            addToMap(commands[1], commands[2], curMap, curMap_reverse);
        }
        else if(commands[0].equals("GET")) {
            System.out.println(curMap.get(commands[1]));
        }
        else if(commands[0].equals("UNSET")) {
            removeFromMap(commands[1], curMap, curMap_reverse);
        }
        else if(commands[0].equals("NUMEQUALTO")) {
            System.out.println(curMap_reverse.get(commands[1]));
        }
        else{
            System.out.println("INVALID COMMAND");
        }

    }

    private static void removeFromMap(String key, HashMap<String, String> curMap, HashMap<String, Integer> curMap_reverse) {
        String value = curMap.get(key);
        curMap.remove(key);
        removeFromReverseMap(value,curMap_reverse);

    }

    private static void removeFromReverseMap(String value, HashMap<String, Integer> curMap_reverse ) {
        if(curMap_reverse.containsKey(value)) {
            int count = curMap_reverse.get(value);
            if (count == 1)
                curMap_reverse.remove(value);
            else
                curMap_reverse.put(value, count - 1);
        }
    }

    private static void addToMap(String key, String value, HashMap<String, String> curMap, HashMap<String, Integer> curMap_reverse) {

        if(curMap.containsKey(key))
            removeFromReverseMap(curMap.get(key),curMap_reverse);
        curMap.put(key,value);
        addToReverseMap(value,curMap_reverse);

    }

    private static void addToReverseMap(String value, HashMap<String, Integer> curMap_reverse) {
        if(curMap_reverse.containsKey(value))
            curMap_reverse.put(value,curMap_reverse.get(value)+1);
        else
            curMap_reverse.put(value,1);
    }


}
