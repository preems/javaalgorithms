package com.learn.problem;

import com.learn.IO;
import com.learn.linkedlist.Node;
import sun.jvm.hotspot.debugger.windbg.DLL;
import sun.security.x509.IPAddressName;

import java.util.HashMap;
import java.util.Vector;

/**
 * Created by Preetham MS <me@preetham.in> on 8/26/15.
 */
public class LRUCache<K,V> {

    int capacity;
    private HashMap<K,DllNode> map;
    private DllNode head=null;
    private DllNode tail=null;

    LRUCache(int capacity){
        this.capacity=capacity;
        map = new HashMap<>();
    }


    public V get(K key) {
        DllNode node = map.get(key);
        moveToHead(node);
        return (V)node.value;

    }

    private void moveToHead(DllNode node) {
        if(head==node)
            return;

        if(tail==node){
            tail=tail.prev;
        }

        if(node.prev!=null){
            node.prev.next=node.next;
        }

        if(node.next!=null) {
            node.next.prev=node.prev;
        }

        node.next=head;
        node.prev=null;

        if(head!=null) {
            head.prev=node;

        }
        head=node;
        if(tail==null)
            tail=node;
    }


    public void set(K key, V value) {
        if(map.containsKey(key)) {
            DllNode node = map.get(key);
            node.value=value;
            moveToHead(node);
        }
        else{
            DllNode node = new DllNode(key,value);

            moveToHead(node);
            map.put(key,node);

            if(map.size()>capacity) {
                removeTail();
            }

        }
    }

    private void removeTail() {
        if(tail!=null)
        {
            tail=tail.prev;
            tail.next=null;
        }
    }

    public void printList() {
        //Use for debug only
        DllNode cur=head;
        while(cur!=null){
            IO.p(cur.key+" ");
            cur=cur.next;
        }
        IO.pln(" ");
    }

    public int size(){
        int size=0;
        DllNode cur=head;
        while(cur!=null){
            size++;
            cur=cur.next;
        }
        return size;
    }


}

class DllNode<K,V> {
    K key;
    V value;
    DllNode prev;
    DllNode next;


    DllNode(K key, V value) {
        this.key=key;
        this.value=value;
        prev=null;
        next=null;
    }
}

class LRUCacheApp {
    public static void main(String[] args) {
        LRUCache<Integer,Integer> cache = new LRUCache<>(5);
        cache.set(1,10);
        IO.pln(cache.size());
       cache.set(1, 10);
        IO.pln(cache.size());
        cache.set(5, 50);
        cache.set(2, 20);
        cache.set(3, 30);
        cache.set(4, 40);
        IO.pln(cache.size());
        cache.printList();
        IO.pln(cache.get(2));
        cache.printList();
        IO.pln(cache.size());
        cache.set(1, 11);
        IO.pln(cache.get(1));
        cache.printList();
        cache.set(7, 70);
        IO.pln(cache.size());
        cache.printList();
        cache.set(8, 80);
        IO.pln(cache.size());
        cache.printList();

    }
}
