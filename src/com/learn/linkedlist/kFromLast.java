package com.learn.linkedlist;

/**
 * Created by preems on 4/14/15.
 */
public class kFromLast {
    public static void main(String[] args) {
        Node head =Utils.generateList(20);
        Utils.printList(head);
        reccursivekfromlast(head, 8);
        iterativekfromlast(head,8);
    }

    private static int reccursivekfromlast(Node head, int k) {

        if(head==null) {
            return 1;
        }

        int i = reccursivekfromlast(head.next,k);
        if(i==k) {
            System.out.println("Reccursive k form last is "+head.data);
        }

        return i+1;

    }

    private static void iterativekfromlast(Node head,int k) {
        Node front =head;
        Node cur = head;
        for(int i=0;i<k;i++) {
            front=front.next;
        }

        while(front!=null){
            front=front.next;
            cur = cur.next;
        }

        System.out.println("Iterative Kth from last is ="+cur.data);
    }


}
