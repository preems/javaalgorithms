package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 6/29/15.
 */
public class deleteNodeWithHigherOnRight {

    public static void main(String[] args) {
        Node head = Utils.generateList(15);
        Utils.printList(head);
        head = delete(head);
        Utils.printList(head);
    }

    private static Node delete(Node head) {

        Node reversed = reverse(head);

        int max=0;

        Node cur =reversed;
        Node prev=null;
        while(cur!=null) {
            if (cur.data>=max) {
                max=cur.data;
                prev=cur;
                cur=cur.next;
            }
            else {
                prev.next=cur.next;
                cur=cur.next;
            }

        }

        return reverse(reversed);

    }

    private static Node reverse(Node root) {

        Node nex = root.next;
        Node cur = root;
        Node prev = null;

        while(cur!=null) {
            cur.next = prev;
            prev = cur;
            cur = nex;
            if (nex!=null)
                nex= nex.next;
        }
        return prev;

    }
}
