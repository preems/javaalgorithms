package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 6/29/15.
 */
public class moveOddtoEnd {

    public static void main(String[] args) throws InterruptedException{
        Node head = Utils.generateList(10);
        Utils.printList(head);

        head =seperate(head);
        Utils.printList(head);
    }

    private static Node seperate(Node head) throws InterruptedException{
        Node tail = head;
        int count=0;
        while(tail.next!=null) {
            tail=tail.next;
            count++;
        }
        count++; //Accodomate for not counting last element

        while(head.data%2==1) {
            tail.next=head;
            head=head.next;
            tail=tail.next;
            tail.next=null;
        }
        //return head;

        Node cur = head;
        Node prev=null;
        while(count>0) {
            Thread.sleep(1000);
            Utils.printList(head);
            if(cur.data%2==1) {
                tail.next=cur;
                prev.next=cur.next;
                cur=cur.next;
                tail=tail.next;
                tail.next=null;
            }
            else {
                prev=cur;
                cur=cur.next;
            }
            count--;
        }

        return head;
    }
}
