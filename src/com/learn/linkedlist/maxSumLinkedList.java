package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 8/4/15.
 */
public class maxSumLinkedList {
    //http://www.geeksforgeeks.org/maximum-sum-linked-list-two-sorted-linked-lists-common-nodes/

    public static void main(String[] args) {
        Node head1 = Utils.generateSortedList(10);
        Node head2 = Utils.generateSortedList(10);
        Utils.printList(head1);
        Utils.printList(head2);
        Node res = getmaxSumLinkedList(head1,head2);
        Utils.printList(res);

    }

    private static Node getmaxSumLinkedList(Node head1, Node head2) {
        Node head = new Node(0);
        Node temp = head;
        int sum1=0,sum2=0;
        Node prev1,prev2;

        while(head1!=null && head2!=null ) {

            prev1=head1;
            prev2=head2;

            while( (head1!=null && head2!=null ) && head1.data!=head2.data) {
                sum1+=head1.data;
                head1=head1.next;
                sum2+=head2.data;
                head2=head2.next;
            }
            if(sum1>sum2) {
                head.next=prev1;
            }
            else {
                head.next=prev2;
            }

            while(head.next!=null)
                head=head.next;

            if (head1!=null)
            head1=head1.next;
            if (head2!=null)
            head2=head2.next;
            sum1=sum2=0;

        }

        return temp.next;


    }
}
