package com.learn.linkedlist;

import java.util.Random;

/**
 * Created by Preetham MS <me@preetham.in> on 6/30/15.
 */
public class deleteGivenNode {

    public static void main(String[] args) {
        Node head = Utils.generateList(10);
        Utils.printList(head);
        Node cur = head;
        Random random = new Random();

        for (int i =0;i<random.nextInt(10);i++) {
            cur=cur.next;
        }

        System.out.println(cur.data);
        //delete(head, cur);
        deleteSimple(cur);
        Utils.printList(head);
    }

    private static void deleteSimple(Node cur) {
        cur.data=cur.next.data;
        cur.next=cur.next.next;
    }

    private static void delete(Node head, Node node) {
        if(head==node) {
            head.data=head.next.data;
            head.next=head.next.next;
            return;
        }

        else {
            Node prev =head;
            while(prev.next!=null && prev.next!=node) {
                prev=prev.next;
            }
            prev.next=prev.next.next;
            return;
        }
    }
}
