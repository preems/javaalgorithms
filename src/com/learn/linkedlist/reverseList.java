package com.learn.linkedlist;

import com.learn.linkedlist.Utils;
import com.learn.linkedlist.Node;
/**
 * Created by preems on 4/8/15.
 */
public class reverseList {

    public static void main(String[] args) {
        Node root = Utils.generateList(10);
        Utils.printList(root);
        root = reverse(root);
        Utils.printList(root);

        printReverse(root);
    }

    private static Node reverse(Node root) {

        Node nex = root.next;
        Node cur = root;
        Node prev = null;

        while(cur!=null) {
            cur.next = prev;
            prev = cur;
            cur = nex;
            if (nex!=null)
                nex= nex.next;
        }
        return prev;

    }

    public static void printReverse(Node root) {
        /*  This prints list in a reccursive way without modifying the list.
        *
        */
        if (root==null) {
            return;
        }
        printReverse(root.next);
        System.out.print(root.data+"-->");
    }
}
