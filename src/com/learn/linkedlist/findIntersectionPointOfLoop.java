package com.learn.linkedlist;

import com.learn.IO;

import java.util.Random;

/**
 * Created by Preetham MS <me@preetham.in> on 6/30/15.
 */
public class findIntersectionPointOfLoop {
    public static void main(String[] args) {
        Node head = Utils.generateList(15);
        Utils.printList(head);
        Node cur = head;
        Random random = new Random();

        for (int i =0;i<random.nextInt(10);i++) {
            cur=cur.next;
        }
        Node tail = head;
        while(tail.next!=null) tail=tail.next;

        tail.next=cur;
        IO io = new IO();
        io.pln(cur.data);

        io.pln(find(head).data);
    }

    private static Node find(Node head) {
        Node fast = head.next.next;
        Node slow = head;

        while(fast!=slow) {
            fast=fast.next.next;
            slow=slow.next;
        }

        System.out.println(slow.data+" "+fast.data);

        //Get loop size
        int loopsize=1;
        slow=slow.next;
        while(slow!=fast) {
            slow=slow.next;
            loopsize++;
        }
        System.out.println(slow.data+" "+fast.data);

        //
        fast=head;
        slow=head;
        while(loopsize>0) {
            fast=fast.next;
            loopsize--;
        }

        while(slow!=fast) {
            slow=slow.next;
            fast=fast.next;
        }

        return slow;


    }
}
