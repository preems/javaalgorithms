package com.learn.linkedlist;

import com.sun.tools.corba.se.idl.constExpr.Not;

/**
 * Created by preems on 4/8/15.
 */
public class listBasics {

    public static void main(String[] args) {
        Node head = Utils.generateList(10);

        Node node = head;

        Utils.printList(head);
        if (node==head) head=head.next;
        deleteNode(node);
        Utils.printList(head);

        head = insertAtHead(head,5);
        insertAtLast(head, 10);

        Utils.printList(head);

        head = Utils.generateList(6);
        Utils.printList(head);
        printMiddle(head);
        deleteList(head);
    }

    private static void insertAtLast(Node head, int i) {
        while(head.next!=null) {
            head=head.next;
        }

        Node node = new Node(i);
        head.next=node;

    }

    private static Node insertAtHead(Node head, int i) {
        Node node = new Node(i);
        node.next=head;
        return node;
    }

    private static void deleteNode(Node node) {

        if (node.next!=null) {
            Node nex = node.next;
            node.next=nex.next;
            node.data=nex.data;
            nex=null;
        }
    }

    private static void printMiddle(Node head) {
        if (head ==null) {
            return;
        }

        Node fast = head;
        Node slow = head;

        while(fast!=null && fast.next!=null) {
            fast = fast.next.next;
            slow = slow.next;
        }

        System.out.println("Middle=" + slow.data);
    }

    private static void deleteList(Node head) {
        if(head==null)
        {
            return;
        }

        Node back = head;

        while(back!=null){
            Node temp=back;
            back=back.next;
            temp.next=null;
        }
    }
}
