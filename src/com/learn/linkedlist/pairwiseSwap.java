package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */
public class pairwiseSwap {
    public static void main(String[] args) {
        Node head = Utils.generateList(11);
        Utils.printList(head);
        head = swap(head);
        Utils.printList(head);
    }

    private static Node swap(Node head) {

        if(head==null || head.next==null) {
            return head;
        }
        else {
            //int temp = head.data;
            //head.data = head.next.data;
            //head.next.data = temp;
            Node third = head.next.next;
            head.next.next=head;
            Node second = head.next;
            head.next= swap(third);

            return second;
        }

    }
}
