package com.learn.linkedlist;

import java.util.Stack;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */
public class isPalindrome {
    public static void main(String[] args) {
        Node head = Utils.generateList(2);
        Utils.printList(head);
        System.out.println(isPal(head));
        System.out.println(isPalByReversing(head));
    }


    private static boolean isPal(Node head) {

        Stack<Integer> stack = new Stack<>();
        Node cur = head;

        while(cur!=null) {
            stack.add(head.data);
            cur = cur.next;
        }

        cur = head;
        while(!stack.empty()) {
            if(stack.pop()!=cur.data) {
                return false;
            }
            cur=cur.next;
        }

        return true;

    }

    private static boolean isPalByReversing(Node head) {
        Node fast=head;
        Node slow=head;

        while( fast!=null &&fast.next!=null  ) {
            fast=fast.next.next;
            slow=slow.next;
        }


        slow=reverse(slow);

        fast=head;
        while(slow!=null && fast!=null)
        {
            if(slow.data!=fast.data)
                return false;
            slow=slow.next;
            fast=fast.next;
        }
        return true;

    }

    private static Node reverse(Node head) {

        if(head.next==null)
            return head;

        Node prev=null;
        Node cur=head;
        Node next=head;

        while(next!=null) {
            next=next.next;
            cur.next=prev;
            prev=cur;
            cur=next;
        }
        return cur;

    }

}
