package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */
public class moveLastToFirst {

    public static void main(String[] args) {
        Node head = Utils.generateList(10);
        Utils.printList(head);
        head = move(head);
        Utils.printList(head);
        Utils.printList(move(null));
    }

    private static Node move(Node head) {

        if(head==null || head.next==null) {
            return head;
        }

        Node newhead = new Node();
        newhead.next = head;
        Node cur = head;



        while(cur.next.next!=null) {
            cur=cur.next;
        }

        newhead.data=cur.next.data;
        cur.next=null;
        return newhead;
    }
}
