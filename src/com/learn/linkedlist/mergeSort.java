package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 8/3/15.
 */
public class mergeSort {
    public static void main(String[] args) {
        Node head = Utils.generateList(20);
        Utils.printList(head);
        head = MergeSort(head);
        Utils.printList(head);

    }

    private static Node MergeSort(Node head) {

        int size = Utils.length(head);

        Node[] splitLists;

        if(head==null) {
            return null;
        }

        if (size>2) {
            splitLists=splitList(head,size);

            head = merge(MergeSort(splitLists[0]), MergeSort(splitLists[1]));
            return head;
        }
        else if(size==2) {
            if(head.data<=head.next.data) {
                return head;
            }
            else {
                head.next.next=head;
                Node temp = head.next;
                head.next=null;
                return temp;
            }
        }
        else {
            return head;
        }



    }

    private static Node merge(Node node1, Node node) {
        Node head = new Node();
        Node ohead = head;

        while((node1!=null) && (node!=null)) {
            if(node.data<node1.data) {
                Node temp = new Node(node.data);
                head.next=temp;
                head=head.next;
                node=node.next;
            }
            else {
                Node temp = new Node(node1.data);
                head.next=temp;
                head=head.next;
                node1=node1.next;
            }

        }

        while(node!=null) {
            Node temp = new Node(node.data);
            head.next=temp;
            head=head.next;
            node=node.next;
        }

        while(node1!=null) {
            Node temp = new Node(node1.data);
            head.next=temp;
            head=head.next;
            node1=node1.next;
        }

        head.next=null;

        return ohead.next;

    }

    private static Node[] splitList(Node head,int size) {
        Node[] lists = new Node[2];
        Node prev = null;
        size = size/2;
        lists[0]=head;

        for(int i =0;i<size;i++) {
            prev = head;
            head=head.next;
        }
        prev.next=null;


        lists[1]=head;

        return lists;
    }
}
