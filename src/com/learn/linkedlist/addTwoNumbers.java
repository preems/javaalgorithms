package com.learn.linkedlist;

import java.util.Random;

/**
 * Created by Preetham MS <me@preetham.in> on 6/30/15.
 */
public class addTwoNumbers {
    public static void main(String[] args) {
        Random random = new Random();
        Node head1 = Utils.generateList(random.nextInt(10));
        Node head2 = Utils.generateList(random.nextInt(10));

        Utils.printList(head1);
        Utils.printList(head2);

        Utils.printList(add(head1,head2));
    }

    private static Node add(Node head1, Node head2) {
        Node head = new Node();
        Node cur = head;
        int sum,carry;
        carry=0;
        while(head1!=null && head2!=null) {
            sum=head1.data+head2.data+carry;
            carry=sum/10;
            Node temp = new Node(sum%10);
            cur.next=temp;
            cur=cur.next;
            head1=head1.next;
            head2=head2.next;
        }

        while (head1!=null) {
            sum=head1.data+carry;
            carry=sum/10;
            Node temp = new Node(sum%10);
            cur.next=temp;
            cur=cur.next;
            head1=head1.next;
        }

        while (head2!=null) {
            sum=head2.data+carry;
            carry=sum/10;
            Node temp = new Node(sum%10);
            temp.next=null;
            cur.next=temp;
            cur=cur.next;
            head2=head2.next;
        }

        return head.next;

    }
}
