package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */
public class reverseInGroup {
    public static void main(String[] args) {

        Node head = Utils.generateList(12);
        Utils.printList(head);
        head = reverse(head, 4);
        Utils.printList(head);
    }

    private static Node reverse(Node head, int k) {

        Node cur = head;

        if(cur==null) {
            return null;
        }

        Node prev=null;
        Node next=cur.next;

        int count=k;
        while(cur!=null && count>0) {

            next=cur.next;
            cur.next=prev;
            prev=cur;
            cur=next;
            count--;
        }

        if(next!=null) {
            head.next=reverse(cur,k);
        }

        return prev;

    }
}
