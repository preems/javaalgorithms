package com.learn.linkedlist;

import java.util.Random;

/**
 * Created by preems on 4/8/15.
 */
public class Utils {

    public static Node generateList(int size) {
        Random rand = new Random();
        Node node=null;
        int i=0;
        while (i<size) {
            Node temp = new Node(rand.nextInt(20));
            temp.next = node;
            node = temp;
            i++;
        }

        return node;
    }

    public static Node generateSortedList(int size) {
        Random random = new Random();

        Node node=null;
        int i =0;
        int lastInt=1000;
        while(i<size) {
            int data = random.nextInt(lastInt+1);

            lastInt=data;
            Node temp = new Node(data);
            temp.next=node;
            node=temp;
            i++;
        }
        return node;
    }

    public static void printList(Node head) {
        while(head!=null) {
            System.out.print(head.data+"-->");
            head=head.next;
        }
        System.out.println("null");
    }

    public static int length(Node head) {
        int l =0;
        while(head!=null) {
            head=head.next;
            l++;
        }

        return l;

    }

    public static void main(String[] args) {
        printList(generateList(10));
    }
}
