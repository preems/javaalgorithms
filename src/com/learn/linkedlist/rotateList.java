package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 8/5/15.
 */
public class rotateList {
    //http://www.geeksforgeeks.org/rotate-a-linked-list/

    public static void main(String[] args) {
        Node head = Utils.generateList(10);
        Utils.printList(head);
        int k = 3;
        Node res = rotate(head,k);
        Utils.printList(res);
    }

    private static Node rotate(Node head, int k) {
        Node oldHead = head;
        Node temp=head,newHead;
        for(int i=0;i<k;i++) {
            head=head.next;
            temp=head;
        }
        head=head.next;
        newHead=head;

        temp.next=null;
        while(head.next!=null) {
            head=head.next;
        }
        head.next=oldHead;
        return newHead;



    }
}
