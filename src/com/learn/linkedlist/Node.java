package com.learn.linkedlist;

import java.util.Random;

/**
 * Created by preems on 4/8/15.
 */
public class Node {
    public int data;
    public Node next;

    public Node(int data) {
        this.data=data;
        this.next=null;
    }

    public Node() {
        this.next=null;
    }




}
