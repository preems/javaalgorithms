package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */
public class intersectionOfSortedLists {
    public static void main(String[] args) {
        Node head1 = Utils.generateSortedList(20);
        Utils.printList(head1);
        Node head2 = Utils.generateSortedList(20);
        Utils.printList(head2);

        Utils.printList(intersectSortedLists(head1,head2));
    }

    private static Node intersectSortedLists(Node head1, Node head2) {
        Node sortedHead = new Node(); //Dummy first node
        Node cur = sortedHead;
        while(head1!=null && head2!=null) {
            if(head1.data==head2.data) {
                Node temp = new Node(head1.data);
                temp.next=null;
                cur.next=temp;
                cur=cur.next;
                head1=head1.next;
                head2=head2.next;
            }
            else if(head1.data<head2.data) {
                head1=head1.next;
            }
            else {
                head2=head2.next;
            }
        }

        return sortedHead.next;
    }
}
