package com.learn.linkedlist;

/**
 * Created by preems on 4/14/15.
 */
public class paritionOnX {

    public static void main(String[] args) {

        //Write code to partition a linked list around a value x, such that all nodes less than x come before alt nodes greater than or equal to x.

        Node head = Utils.generateList(20);
        Utils.printList(head);
        int x=10;

        Node leftStart=null;
        Node rightStart=null;
        Node left=null;
        Node right=null;

        while (head!=null) {
            if (head.data<x) {
                if(leftStart==null) {
                    leftStart=head;
                    left=leftStart;
                }
                else {
                    left.next=head;
                    left=left.next;
                }
            }
            else {
                if(rightStart==null) {
                    rightStart=head;
                    right=rightStart;
                }
                else {
                    right.next=head;
                    right=right.next;
                }
            }
            head=head.next;
        }

        right.next=null;
        left.next=rightStart;

        //Return the leftStart
        Utils.printList(leftStart);
    }
}