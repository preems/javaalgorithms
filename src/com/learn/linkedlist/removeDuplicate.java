package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */
public class removeDuplicate {

    public static void main(String[] args) {
        // Input should be Sorted list.

        Node head = Utils.generateSortedList(10);
        Utils.printList(head);
        Node cur = head;
        while(cur.next!=null) {
            if(cur.data==cur.next.data) {
                cur.next=cur.next.next;
            }
            else {
                cur = cur.next;
            }
        }

        Utils.printList(head);
    }
}