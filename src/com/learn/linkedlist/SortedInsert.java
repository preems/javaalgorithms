package com.learn.linkedlist;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */
public class SortedInsert {

    public static void main(String[] args) {
        Node head = new Node(5);
        Node cur =head;
        Node next = new Node(6);
        cur.next=next;
        cur=cur.next;
        next = new Node(8);
        cur.next=next;

        int i = 10;

        Utils.printList(sortedInsert(Utils.generateList(20), i));

    }

    private static Node sortedInsert(Node head, int i) {
        if(head==null) {
            return new Node(i);
        }

        Node cur = head;
        while(cur.next!=null) {
            if (i<cur.next.data) {
                cur=cur.next;
            }
            else {
                Node temp = new Node(i);
                temp.next=cur.next;
                cur.next=temp;
                return head;
            }
        }

        return head;
    }
}
