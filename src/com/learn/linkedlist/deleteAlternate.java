package com.learn.linkedlist;

/**
 * Created by Preetham MS <me@preetham.in> on 6/28/15.
 */
public class deleteAlternate {

    public static void main(String[] args) {
        Node head = Utils.generateList(10);
        Utils.printList(head);
        head = iterativedelete(head);
        Utils.printList(head);
        head = reccursivedelete(head);
        Utils.printList(head);
    }

    private static Node reccursivedelete(Node head) {

        if(head==null || head.next==null) {
            return head;
        }

        head.next=reccursivedelete(head.next.next);

        return head;
    }

    private static Node iterativedelete(Node head) {

        if(head==null || head.next==null) {
            return head;
        }


        Node Head = head;
        while(head!=null && head.next!=null) {
            head.next=head.next.next;
            head=head.next;
        }
        return Head;
    }
}
