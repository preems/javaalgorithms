package com.learn.algorithm;

import com.learn.IO;
import com.learn.array.Utils;

import java.util.Arrays;

/**
 * Created by Preetham MS <me@preetham.in> on 8/13/15.
 */
public class mergeSort {

    public static void main(String[] args) {
        int[] a =Utils.generateIntArray(20);
        Utils.printIntArray(a);
        a=sort(a);
        Utils.printIntArray(a);
    }

    private static int[] sort(int[] a) {
        Utils.printIntArray(a);
        int n = a.length;

        if(n==1) {
            return a;
        }
        if(n==2) {
            if(a[0]<=a[1])
                return a;
            else {
                int temp = a[1];
                a[1]=a[0];
                a[0]=temp;
                return a;
            }
        }
        else {

            return merge(sort(Arrays.copyOfRange(a, 0, n / 2)), sort(Arrays.copyOfRange(a, (n / 2), n)));
        }


    }

    private static int[] merge(int[] a, int[] b) {
        int[] res = new int[a.length+b.length];

        int j=0,k=0;
        int i=0;
        while(j<a.length && k<b.length) {
            if(a[j]<=b[k]) {
                res[i]=a[j];
                i++;
                j++;
            }
            else {
                res[i]=b[k];
                i++;
                k++;
            }
        }

        if(j!=a.length) {
            while(j!=a.length) {
                res[i] = a[j];
                i++;
                j++;
            }
        }

        if(k!=b.length) {
            while(k!=b.length) {
                res[i]=b[k];
                i++;
                k++;
            }
        }

        return res;

    }
}
