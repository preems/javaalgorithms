package com.learn.algorithm;

import com.learn.array.Utils;

/**
 * Created by Preetham MS <me@preetham.in> on 8/16/15.
 */
public class insertionSort {

    public static void main(String[] args) {

        int[] array = Utils.generateIntArray(20);
        Utils.printIntArray(array);
        array = sort(array);
        Utils.printIntArray(array);
    }

    private static int[] sort(int[] array) {
        int n = array.length;

        for( int i=1;i<n;i++) {
            for (int k=i;k>0;k--) {
                if(array[k]<array[k-1]) {
                    int temp = array[k-1];
                    array[k-1] = array[k];
                    array[k]=temp;
                }
            }
        }

        return array;

    }


}
