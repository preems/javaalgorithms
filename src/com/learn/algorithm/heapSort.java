package com.learn.algorithm;

import com.learn.IO;
import com.learn.array.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Preetham MS <me@preetham.in> on 8/23/15.
 */
public class heapSort {


    public static void main(String[] args) {

        int arr[] = Utils.generateIntArray(10);
        Utils.printIntArray(arr);
        int n=arr.length;
        for(int i=n-1;i>=0;i--)
            arr = heapify(arr,i,n);

        Utils.printIntArray(arr);

        for(int i=n-1;i>0;i--) {
            int temp = arr[i];
            arr[i]=arr[0];
            arr[0]=temp;
            arr=heapify(arr,0,i);
        }
        Utils.printIntArray(arr);

    }

    private static int[] heapify(int[] arr, int i, int n) {

        int l =2*i+1;
        int r =2*i+2;
        int largest;

        if(r<n){
            if(arr[r]>arr[l])
                largest=r;
            else
                largest=l;
            if(arr[largest]<arr[i])
                largest=i;
        }
        else if( (l<n) &&(arr[l]>arr[i])) {
            largest=l;
        }
        else
            largest=i;

        if(largest!=i)
        {
            int temp = arr[largest];
            arr[largest]=arr[i];
            arr[i]=temp;
            arr=heapify(arr,largest,n);
        }
        return arr;

    }
}
