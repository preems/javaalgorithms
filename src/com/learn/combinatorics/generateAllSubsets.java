package com.learn.combinatorics;

import com.learn.IO;
import com.learn.array.Utils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Preetham MS <me@preetham.in> on 8/30/15.
 */
public class generateAllSubsets {
    //Generate all possible subsets

    public static void main(String[] args) {
        ArrayList<Integer> set = new ArrayList<>();
        set.add(4);
        set.add(5);
        set.add(9);
        set.add(7);
        set.add(3);

        //ArrayList<ArrayList<Integer>> subsets = generatesubsets(set,set.size());
        ArrayList<ArrayList<Integer>> subsets = generatesubsetsUsingBits(set, set.size());
        System.out.println(subsets.get(31));
    }

    private static ArrayList<ArrayList<Integer>> generatesubsetsUsingBits(ArrayList<Integer> set, int size) {
        ArrayList<ArrayList<Integer>> allSubsets = new ArrayList<>();
        int x=1;
        x=x<<size;
        for(int i=x-1;i>=0;i--){
            ArrayList<Integer> newSet = new ArrayList<>();
            int index=0;
            int j=i;
            while(j>0){
                if((j&1)==1){
                    newSet.add(set.get(index));
                }
                j=j>>1;
                index++;
            }
            allSubsets.add(newSet);
        }


        return allSubsets;
    }

    private static ArrayList<ArrayList<Integer>> generatesubsets(ArrayList<Integer> set,int size) {
        ArrayList<ArrayList<Integer>> allSubsets;

        if(size==0){
            allSubsets = new ArrayList<>();
            allSubsets.add(new ArrayList<>());
        }
        else {
            allSubsets=generatesubsets(set,size-1);
            int element = set.get(size-1);
            ArrayList<ArrayList<Integer>> newSets = new ArrayList<>();
            for(ArrayList<Integer> list: allSubsets){
                ArrayList<Integer> newSet = new ArrayList<>();
                newSet.addAll(list);
                list.add(element);
                newSets.add(newSet);
            }
            allSubsets.addAll(newSets);
        }

        return allSubsets;
    }
}
