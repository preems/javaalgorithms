package com.learn.dp;

import com.learn.IO;
import com.learn.array.Utils;

/**
 * Created by Preetham MS <me@preetham.in> on 8/15/15.
 */
public class coinChange {
    //http://www.geeksforgeeks.org/dynamic-programming-set-7-coin-change/

    public static void main(String[] args) {
        int[] Set = {1,5,20,50};
        int N = 100;
        IO.pln(ways(Set,N));
    }

    private static int ways(int[] set, int n) {
        int[][] table = new int[n+1][set.length+1];


        for(int[] row:table){
            row[0]=0;
            //row[1]=1;
        }

        for(int i=0;i<set.length+1;i++) {
            table[0][i]=1;
        }


        for(int i=1;i<=n;i++) {
            for(int j=1;j<=set.length;j++) {

                if(i>=set[j-1])
                    table[i][j]=table[i][j-1]+table[i-set[j-1]][j];
                else
                    table[i][j]=table[i][j-1];
            }
        }

        Utils.print2dIntArray(table);
        return table[n][set.length];

    }
}
