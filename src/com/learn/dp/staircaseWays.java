package com.learn.dp;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/29/15.
 */
public class staircaseWays {
    //No of ways a staircase can be climbed if climbed in 1,2 or 3 steps.

    static int table[];

    public static void main(String[] args) {
        int n=10;
        table = new int[n+1];
        table[0]=1;
        table[1]=1;
        table[2]=2;
        for(int i=3;i<=n;i++){
            table[i]=table[i-1]+table[i-2]+table[i-3];
        }
        IO.pln(table[n]);
    }

}
