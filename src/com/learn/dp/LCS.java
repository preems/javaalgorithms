package com.learn.dp;

import com.learn.IO;
import com.learn.string.Utils;

/**
 * Created by Preetham MS <me@preetham.in> on 8/14/15.
 */
public class LCS {
    public static void main(String[] args) {
        //String a = Utils.generateString(10,10);
        //String b = Utils.generateString(5,10);
        String a ="ABCDGH";
        String b = "AEDFHR";

        IO.pln(a);
        IO.pln(b);

        IO.pln(getLCS(a,b));

    }

    private static int getLCS(String a, String b) {
        int table[][] = new int[a.length()+1][b.length()+1];

        for(int i=1;i<=a.length();i++){
            for(int j=1;j<=b.length();j++) {
                if(a.charAt(i-1)==b.charAt(j-1)) {
                    table[i][j]=table[i-1][j-1]+1;
                }
                else {
                    table[i][j] = Math.max(table[i-1][j],table[i][j-1]);
                }
            }
        }

        com.learn.array.Utils.print2dIntArray(table);
    return table[a.length()][b.length()];

    }
}
