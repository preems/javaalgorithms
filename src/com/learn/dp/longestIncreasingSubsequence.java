package com.learn.dp;

import com.learn.IO;
import com.learn.array.Utils;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * Created by Preetham MS <me@preetham.in> on 8/14/15.
 */
public class longestIncreasingSubsequence {

    //http://www.geeksforgeeks.org/dynamic-programming-set-3-longest-increasing-subsequence/
    public static void main(String[] args) {
        int[] arr = Utils.generateIntArray(10);
        //int[] arr = {7,8,9,5,7,5,12,2,6,3};
        Utils.printIntArray(arr);
        IO.pln(lis(arr));
    }

    private static int lis(int[] arr) {


        int[] table = new int[arr.length];

        for(int i=0;i<arr.length;i++) {
            for(int j=i-1;j>=0;j--) {
                if( (arr[i]>arr[j]) && (table[i]<table[j]+1) ) {
                    table[i]=table[j]+1;
                }
            }
            if(table[i]==0) table[i]=1;
        }

        Utils.printIntArray(table);
        return maxInArray(table);
    }


    private static int maxInArray(int[] a) {
        int max=0;
        for(int e : a) {
            if(e>max) {
                max=e;
            }
        }
        return max;
    }
}
