package com.learn.dp;

import com.learn.IO;
import com.learn.string.Utils;

/**
 * Created by Preetham MS <me@preetham.in> on 8/14/15.
 */
public class minEditDistance {
    //http://www.geeksforgeeks.org/dynamic-programming-set-5-edit-distance/

    public static void main(String[] args) {
        String a = Utils.generateString(10,5);
        String b = Utils.generateString(7,5);

        //String a = "SATURDAY";
        //String b = "SUNDAY";
        IO.pln(a);
        IO.pln(b);

        IO.pln(distance(a,b));

    }

    private static int distance(String a, String b) {
        int[][] table = new int[a.length()+1][b.length()+1];
        int temp;
        for(int i=0;i<=a.length();i++) {
            table[i][0]=i;
        }

        for(int i=0;i<=b.length();i++) {
            table[0][i]=i;
        }


        for(int i=1;i<=a.length();i++) {
            for(int j=1;j<=b.length();j++) {
                if(a.charAt(i-1)==b.charAt(j-1)) {
                    table[i][j]=table[i-1][j-1];
                }
                else {
                    if(a.substring(0,i-1)==b.substring(0,j-1))
                         temp=0;
                    else
                         temp=1;
                    table[i][j]=min(table[i-1][j]+1,table[i][j-1]+1,table[i-1][j-1]+temp);
                }
            }
        }

        com.learn.array.Utils.print2dIntArray(table);
    return table[a.length()][b.length()];

    }


    private static int min(int a,int b,int c) {
        int min=-1;
        if(a<b)
            min=a;
        else
            min=b;
        if(c<min)
            min=c;
        return min;
    }
}
