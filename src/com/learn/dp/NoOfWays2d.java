package com.learn.dp;

import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/29/15.
 */
public class NoOfWays2d {

    public static void main(String[] args) {
        int n=2;
        int m=2;
        IO.pln(noOfWays2D(n,m));
    }

    private static int noOfWays2D(int n, int m) {
        int table[][] = new int[n+1][m+1];

        for(int i=0;i<=n;i++)
            table[i][0]=1;

        for(int j=0;j<=m;j++)
            table[0][j]=1;

        for(int i=1;i<=n;i++){
            for(int j=1;j<=m;j++) {
                table[i][j]=table[i-1][j]+table[i][j-1];
            }
        }

        return table[n][m];
    }
}
