package com.learn.dp;

import com.learn.IO;
import com.learn.array.Utils;

/**
 * Created by Preetham MS <me@preetham.in> on 8/14/15.
 */
public class minCostPath {
    //http://www.geeksforgeeks.org/dynamic-programming-set-6-min-cost-path/
    // Min cost path from (0,0) to (m,n) in a matrix

    public static void main(String[] args) {

        int[][] matrix = Utils.generate2dIntArray(4,4);
        Utils.print2dIntArray(matrix);
        IO.pln("");
        int m=matrix.length;
        int n=matrix[0].length;

        IO.pln(mincost(matrix,m,n));
    }

    private static int mincost(int[][] matrix, int m, int n) {
        int[][] table = new int[m+1][n+1];


        table[0][0]=0;

        for(int i=1;i<=m;i++){
            table[i][0]=matrix[i-1][0]+table[i-1][0];
        }
        for(int i=1;i<=n;i++) {
            table[0][i]=matrix[0][i-1]+table[0][i-1];
        }

        for(int i=1;i<=m;i++) {
            for(int j=1;j<=n;j++) {
                table[i][j]=min( table[i-1][j-1],table[i][j-1],table[i-1][j])+matrix[i-1][j-1];
            }
        }

        Utils.print2dIntArray(table);
        return table[m][n];


    }

    private static int min(int a,int b,int c) {
        int min=-1;
        if(a<b)
            min=a;
        else
            min=b;
        if(c<min)
            min=c;
        return min;
    }
}
