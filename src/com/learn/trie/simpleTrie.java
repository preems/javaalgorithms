package com.learn.trie;

import org.junit.Assert;
import org.junit.Assert.*;
import com.learn.IO;

/**
 * Created by Preetham MS <me@preetham.in> on 8/22/15.
 */


class TrieNode {
    public int count;
    public TrieNode children[];

    TrieNode() {
        count=0;
        children=new TrieNode[26];
        for(int i =0;i<children.length;i++) {
            children[i]=null;
        }
    }

}
public class simpleTrie {

    //Simple string trie implementation with search,insert.9/

    private static char a = 'a';

    public static void main(String[] args) {

        TrieNode root = new TrieNode();

        String input[] = {"the", "a", "there", "answer", "any", "by", "bye", "their","there"};

        for(String i:input)
            insert(root,i);

        String search[] = {"the", "a", "there", "answer", "any", "by", "bye", "their","preems","thier","answe","answers"};
        for(String i:search)
        IO.pln(search(root, i));


        Assert.assertEquals(search(root,"the"),true);
        Assert.assertEquals(search(root,"thier"),false);
        Assert.assertEquals(search(root,"answers"),false);
        Assert.assertEquals(search(root,"bye"),true);

    }

    private static boolean search(TrieNode root,String string) {
        char charArr[] = string.toCharArray();

        for(char c:charArr) {
            if(root.children[c-a]!=null) {
                root=root.children[c-a];
            }
            else{
                return false;
            }
        }

        if(root.count!=0)
            return true;
        else
            return false;

    }


    private static void insert(TrieNode root, String string) {

        char charArr[] = string.toCharArray();

        for(char c: charArr) {
            if(root.children[c-a]==null) {
                root.children[c-a]= new TrieNode();
            }
            root=root.children[c-a];
        }
        root.count=root.count+1;

    }



}
